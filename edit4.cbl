       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  LISTING9-4.
       AUTHOR.   MMODPOWW.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  STAR  PIC *****.
       01  NUM-STAR   PIC   9.

       PROCEDURE DIVISION.
       BEGIN.
           PERFORM VARYING NUM-STAR FROM 0 BY 1 UNTIL NUM-STAR > 5
              COMPUTE STAR = 10 ** (4 - NUM-STAR)
              INSPECT STAR   CONVERTING "10" TO SPACE
              DISPLAY NUM-STAR " = " STAR
           END-PERFORM
           STOP RUN
           .
         